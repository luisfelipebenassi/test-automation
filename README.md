# test-automation



## Questions

1. What kind of tests would you conduct to make sure it was well built?
	I would do some manual testing to verify if the specs are being respected and to better understand the applicantion, thinking about continues delivery and if I had access to the code of the applicantion I would develop some component tests to run before deploy it to the test environment and I would create some end-to-end test to run on the test environment making sure that everythings is running smoothly together

2. In your opinion what would be the 3 more important things to test here?
	* If it is returning 2 users with id and points as integer and if they have different ids
	* If it is returning the timestamp of the last query
	* If the points and users are changing after 1 minute

3. What would you improve in this spec?
	I would improve: 
	- add a spec to make it clear which user has the higher points
		ex: users need to be in order, higher points comes first
	- add a spec to clarify what to do when they have the same amout of points
		ex: if there are users with the same score, users with lower id will come first 
	- change something about the last timestamp, add it to a new endpoint that will just return the timestamp from the last query or make it clear that is the timestamp from the last query like changing the label from timestamt to last_querry_timestamp


## Runnig the atutomation

I've used ruby with Rspec to do this automation and add a dockerfile to be easier to run it.

I did 7 scenarios in total, testing the types and validating that all spec are being respected.

Premise: 

	- Docker


With docker istalled we will need to build the docker image, in the main folder where the Dockerfile is located run: 

```docker build -t automation/ruby:latest .```

After the build get completed we can run the tests with:

```docker run -it automation/ruby rspec```

and that is it!


## Report

Report with rspec is done automatically to get it with docker you can run the command bellow:

```docker run -v "$(pwd)"/reports:/usr/src/app/reports -it automation/ruby rspec spec --format html --out reports/rspec_results.html```

It will run the tests and output the result to a html report on 'reports' folder.


I already added two reports there, the [automation report](reports/automation_report.html), it is the output with all tests passing, and I added as well a [fail test scenario report](reports/fail_test_report_sample.html), that I forced to show how it display a failed scenario.