FROM ruby

ENV APP_HOME=/usr/src/app

COPY . $APP_HOME

WORKDIR $APP_HOME
 
RUN gem install bundler
RUN bundle install