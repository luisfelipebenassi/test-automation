describe 'top two max points' do

	it 'validates the status code' do 
		top_two = TopTwo.get('')
		expect(top_two.code).to be 200
	end

	it 'validates id and points type' do
		top_two = JSON.parse(TopTwo.get('').body)
		top_two['users'].each do |user|
			expect(user['id']).to be_an(Integer)
			expect(user['points']).to be_an(Integer)
		end
	end

	it "returns exactly two users" do 
		top_two = JSON.parse(TopTwo.get('').body)
		expect(top_two['users'].count).to be 2
	end 

	it "returns different users" do 
		top_two = JSON.parse(TopTwo.get('').body)
		first_user = top_two['users'][0]
		second_user = top_two['users'][1]
		expect(first_user['id']).not_to eq(second_user['id'])
	end

	it 'validates timestamp' do
		top_two = JSON.parse(TopTwo.get('').body)
		expect(top_two['timestamp']).to be_a(String)

		begin
   			Date.parse(top_two['timestamp'])
		rescue ArgumentError
			raise top_two['timestamp'] + " is not a valid date"
		end
	end

	it "validates that points are being updated" do
		top_two = JSON.parse(TopTwo.get('').body)
		users = top_two['users']
		begin
			Timeout.timeout(60) do
				while users == JSON.parse(TopTwo.get('').body)['users']
					sleep 5
				end
			end
		rescue StandardError => e
			raise "Points were not updated after 1 min"
		end
	end

	it "returns the timestamp from the last querry" do 
		first_request = TopTwo.get('')
		first_request_datetime = DateTime.parse(first_request.headers["date"])
		second_request_timestamp = DateTime.parse(JSON.parse(TopTwo.get('').body)['timestamp'])
		
		# Treshold 
		# As it is being use the date from the response it can have a second or two difference for the timestamp
		treshold = 2
		datetime_difference = ((first_request_datetime - second_request_timestamp) * 24 * 60 * 60).to_i

		expect(datetime_difference).to be <= treshold
		expect(datetime_difference).to be >= -treshold
	end	
end